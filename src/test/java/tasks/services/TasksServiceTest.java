package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

//import javax.swing.table.TableRowSorter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("TaskService")
class TasksServiceTest {
    private TasksService service;
    private SimpleDateFormat formatter;


    @BeforeEach
    void setUp() throws ParseException {
        formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        ArrayTaskList tasks = new ArrayTaskList();
        tasks.add(new Task("Task 1",
                        formatter.parse("23/03/2022 10:30"),
                        formatter.parse("24/03/2022 10:30"),
                        7
                )
        );
        tasks.add(new Task("Task 2",
                        formatter.parse("25/03/2022 10:30"),
                        formatter.parse("26/03/2022 10:30"),
                        12
                )
        );
        tasks.add(new Task("Task 3",
                        formatter.parse("23/03/2022 10:30"),
                        formatter.parse("23/03/2022 10:30"),
                        24
                )
        );
        // Set all tasks as Active
        tasks.forEach(t -> t.setActive(true));

        // Add inactive task
        tasks.add(new Task("Task 4",
                        formatter.parse("23/03/2022 10:30"),
                        formatter.parse("24/03/2022 10:30"),
                        2
                )
        );

        service = new TasksService(tasks);
    }

    @DisplayName("F02_TC03")
    @Test
    void F02_TC03() throws ParseException {
        // Next time == null
        // Ne asteptam sa nu primit Taskul 4(next time == null) si 3
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("23/03/2022 10:30"),
                formatter.parse("27/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
                asList(
                        new Task("Task 1",
                                formatter.parse("23/03/2022 10:30"),
                                formatter.parse("24/03/2022 10:30"),
                                7
                        ),
                        new Task("Task 2",
                                formatter.parse("25/03/2022 10:30"),
                                formatter.parse("26/03/2022 10:30"),
                                12
                        )
                ));
        // Set expected tasks to active
        expected.forEach(task -> task.setActive(true));
        assertIterableEquals(output, expected);
    }

    @DisplayName("F02_TC04")
    @Test
    void F02_TC04() throws ParseException {
        // Task care are start != end -> linia 8
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("23/03/2022 10:30"),
                formatter.parse("27/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
                asList(
                        new Task("Task 1",
                                formatter.parse("23/03/2022 10:30"),
                                formatter.parse("24/03/2022 10:30"),
                                7
                        ),
                        new Task("Task 2",
                                formatter.parse("25/03/2022 10:30"),
                                formatter.parse("26/03/2022 10:30"),
                                12
                        )
                )
        );
        // Set expected tasks to active
        expected.forEach(task -> task.setActive(true));
        assertIterableEquals(output, expected);
    }

    @DisplayName("F02_TC05")
    @Test
    void F02_TC05() throws ParseException {
        // Sa ne returneze un task ok
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("23/03/2022 10:30"),
                formatter.parse("24/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(Collections.singletonList(
                new Task("Task 1",
                        formatter.parse("23/03/2022 10:30"),
                        formatter.parse("24/03/2022 10:30"),
                        7
                )
        ));
        // Set expected tasks to active
        expected.forEach(task -> task.setActive(true));
        assertIterableEquals(output, expected);
    }


    @DisplayName("F02_TC06")
    @Test
    void F02_TC06() throws ParseException {
        // Data start > end
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("23/03/2022 10:30"),
                formatter.parse("22/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>();

        assertIterableEquals(output, expected);
    }

//    @Nested
//    @DisplayName("incomingTasks")
//    class TestIncomingTasksTest {
//        @BeforeEach
//        void setUp() {
//            formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//        }
//
//        @Nested
//        @DisplayName("Empty Repository")
//        class TestEmptyRepository {
//            // list task goala
//            @BeforeEach
//            public void setUp() {
//                ArrayTaskList tasks = new ArrayTaskList();
//                service = new TasksService(tasks);
//            }
//
//            @DisplayName("F02_TC01")
//            @Test
//            void F02_TC02() throws ParseException {
//                Iterable<Task> output = service.incomingTasks(
//                        formatter.parse("23/03/2022 10:30"),
//                        formatter.parse("22/03/2022 10:30")
//                );
//                Iterable<Task> expected = new ArrayList<>();
//
//                assertIterableEquals(output, expected);
//            }
//        }
//
//        @Nested
//        @DisplayName("Task null in Repository")
//        class TestTaskNullInRepositoryTest {
//            @BeforeEach
//            public void setUp() {
//                ArrayTaskList tasks = new ArrayTaskList();
//                service = new TasksService(tasks);
//            }
//
//            @DisplayName("F02_TC02")
//            @Test
//            void F02_TC02() throws ParseException {
//                // Task nulll -> Un task NU poate sa fie null ptc nu poti adauga task-uri nulle
//                // fail("TASK CANNOT BE NULL");
//                Iterable<Task> output = service.incomingTasks(
//                        formatter.parse("23/03/2022 10:30"),
//                        formatter.parse("22/03/2022 10:30")
//                );
//
//                Iterable<Task> expected = new ArrayList<>();
//                assertIterableEquals(output, expected);
//            }
//        }
//
//        @Nested
//        @DisplayName("Populated Repository")
//        class TestPopulatedRepositoryTest {
//
//        }
//    }
}
