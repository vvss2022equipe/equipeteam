package tasks.services;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.ArgumentMatchers.eq;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.ArrayList;
import static java.util.Arrays.asList;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class IntegrationSRETest {    
    private ArrayTaskList taskList;
    private TasksService service;
    private SimpleDateFormat formatter;

    @BeforeEach
    public void setUp() throws ParseException {
        taskList = new ArrayTaskList();

        formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        // INTEGRATE TASK LIST WITH SERVICE
        taskList.add(new Task("TASK 1",
                formatter.parse("22/03/2022 10:30"),
                formatter.parse("23/03/2022 10:35"),
                1
            )
        );
        taskList.add(new Task("TASK 2",
                formatter.parse("23/03/2022 10:30"),
                formatter.parse("23/03/2022 10:35"),
                1
            )
        );
        taskList.forEach(task -> task.setActive(true));

        service = new TasksService(taskList);
        
    }
    
    @Test
    void testFilterTasks() throws ParseException {
        Iterable<Task> output = service.filterTasks(
                formatter.parse("22/03/2022 10:30"),
                formatter.parse("25/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
            asList(
                new Task("TASK 1",
                    formatter.parse("22/03/2022 10:30"),
                    formatter.parse("23/03/2022 10:35"),
                    1
                ),
            new Task("TASK 2",
                    formatter.parse("23/03/2022 10:30"),
                    formatter.parse("23/03/2022 10:35"),
                    1
                )
            )
        );
        expected.forEach(task -> task.setActive(true));
        assertIterableEquals(output, expected);
    }

    @Test
    void testIncomingTasks() throws ParseException {
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("22/03/2022 10:30"),
                formatter.parse("25/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
            asList(
                new Task("TASK 1",
                    formatter.parse("22/03/2022 10:30"),
                    formatter.parse("23/03/2022 10:35"),
                    1
                ),
            new Task("TASK 2",
                    formatter.parse("23/03/2022 10:30"),
                    formatter.parse("23/03/2022 10:35"),
                    1
                )
            )
        );
        expected.forEach(task -> task.setActive(true));
        assertIterableEquals(output, expected);
    }
}
