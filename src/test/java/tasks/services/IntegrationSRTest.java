package tasks.services;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.ArgumentMatchers.eq;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.util.ArrayList;
import static java.util.Arrays.asList;


@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class IntegrationSRTest {
    @Mock
    private Task task1, task2;
    
    private ArrayTaskList taskList;
    private TasksService service;
    private SimpleDateFormat formatter;

    @BeforeEach
    public void setUp() throws ParseException {
        taskList = new ArrayTaskList();

        formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        
        Mockito.lenient().when(task1.getTitle()).thenReturn("TASK 1");
        Mockito.lenient().when(task2.getTitle()).thenReturn("TASK 2");

        Mockito.lenient().when(task1.isActive()).thenReturn(true);
        Mockito.lenient().when(task2.isActive()).thenReturn(true);

        Mockito.lenient().when(task1.nextTimeAfter(
                eq(formatter.parse("22/03/2022 10:30"))
            )
        ).thenReturn(formatter.parse("23/03/2022 10:35"));

        Mockito.lenient().when(task2.nextTimeAfter(
                eq(formatter.parse("22/03/2022 10:30"))
            )
        ).thenReturn(formatter.parse("23/03/2022 10:35"));

        // INTEGRATE TASK LIST WITH SERVICE
        taskList.add(task1);
        taskList.add(task2);
        
        service = new TasksService(taskList);
        
    }
    
    @Test
    void testFilterTasks() throws ParseException {
        Iterable<Task> output = service.filterTasks(
                formatter.parse("22/03/2022 10:30"),
                formatter.parse("25/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
            asList(task1, task2)
        );
            
        assertIterableEquals(output, expected);
    }

    @Test
    void testIncomingTasks() throws ParseException {
        Iterable<Task> output = service.incomingTasks(
                formatter.parse("22/03/2022 10:30"),
                formatter.parse("25/03/2022 10:30")
        );
        Iterable<Task> expected = new ArrayList<>(
            asList(task1, task2)
        );
            
        assertIterableEquals(output, expected);
    }
}
