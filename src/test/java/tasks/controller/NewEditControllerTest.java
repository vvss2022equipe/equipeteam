package tasks.controller;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tasks.model.Task;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.jupiter.api.Assertions.*;

class NewEditControllerTest {
    private NewEditController controller;
    private Task task;
    private SimpleDateFormat formatter;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @BeforeEach
    void setUp() throws ParseException {
        controller = new NewEditController();
        formatter = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
        task = controller.setData("new task", formatter.parse("Wed Mar 16 08:00:00 GMT 2022"), formatter.parse("Wed Mar 16 08:00:00 GMT 2022"), 30);
    }

    @Tag("constructor-test")
    @Test
    void testTaskCreation() throws ParseException {
        assertEquals(task.getTitle(), "new task");
        assertEquals(task.getFormattedDateStart(), sdf.format(formatter.parse("Wed Mar 16 08:00:00 GMT 2022")));
    }

    @Tag("set-data-test")
    @ParameterizedTest
    @ValueSource(strings = {"a", "aaaaabcdefghijklmno"})
    void setData_BVA_3_4(String title) {
        assertDoesNotThrow(() -> {
            controller.setData(title, task.getStartTime(), task.getEndTime(), task.getRepeatInterval());
        });
    }
    @DisplayName("ECP_1")
    @Tag("set-data-test")
    @Test
    void setData_ECP_1() {
        assertThrows(Exception.class, () -> {
            controller.setData("", null, null, -1);
        });
    }

    @Test
    void setData_ECP_2() {
        assertThrows(Exception.class, () -> {
            controller.setData("abc", formatter.parse("abc"), formatter.parse("Wed Mar 16 08:00:00 EET 2022"), 0);
        });
    }
    @Test
    void setData_ECP_3() {
        assertDoesNotThrow(() -> {
            controller.setData("abc", formatter.parse("Wed Mar 16 08:00:00 EET 2022"), formatter.parse("Wed Mar 16 08:00:01 EET 2022"), 30);
        });
    }
    @Test
    void setData_ECP_4() {
        assertThrows(Exception.class, () -> {
            controller.setData("abc", formatter.parse("Wed Mar 16 08:00:00 EET 2022"), formatter.parse(""), 30);
        });
    }
    @Test
    void setData_ECP_5() {
        assertThrows(Exception.class, () -> {
            controller.setData("aaaaaaaaaaaaaaaaaaaaaaaaaaaaa", formatter.parse("Wed Mar 16 08:00:00 EET 2022"), formatter.parse("Wed Mar 16 08:00:01 EET 2022"), 30);
        });
    }
    @Test
    void setData_BVA_2() {
        assertThrows(Exception.class, () -> {
            controller.setData("abc", formatter.parse("Wed Mar 16 08:00:00 EET 2022"), formatter.parse("Wed Mar 16 07:59:59 EET 2022"), 30);
        });
    }

    @RepeatedTest(5)
    void setData_BVA_5() {
        assertThrows(Exception.class, () -> {
            controller.setData("aaaaabcdefghijklmnaao", formatter.parse("Wed Mar 16 08:00:00 EET 2022"), formatter.parse("Wed Mar 16 08:00:01 EET 2022"), 30);
        });
    }
}