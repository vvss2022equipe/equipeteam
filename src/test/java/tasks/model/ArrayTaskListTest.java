package tasks.model;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ArrayTaskListTest {
    private ArrayTaskList taskList;

    @Mock 
    private Task task1, task2;


    @BeforeEach
    public void setUp() {
        taskList = new ArrayTaskList();
    }

    @Test
    void testAdd() {
        Mockito.when(task1.getTitle()).thenReturn("TASK 1");
        taskList.add(task1);
        String expected = "TASK 1";
        String actual = taskList.getTask(0).getTitle();
        assertEquals(expected, actual);
    }

    @Test
    void testAddAll() {
        Mockito.when(task1.getTitle()).thenReturn("TASK 1");
        Mockito.when(task2.getTitle()).thenReturn("TASK 2");
        taskList.add(task1);
        taskList.add(task2);
        assertEquals("TASK 1", taskList.getTask(0).getTitle());
        assertEquals("TASK 2", taskList.getTask(1).getTitle());
        assertEquals(2, taskList.size());
    }


    @Test
    void testGetTask() {
        Mockito.when(task1.getTitle()).thenReturn("TASK ASDSADSA");
        taskList.add(task2);
        taskList.add(task1);
        String output = taskList.getTask(1).getTitle();
        String expected = "TASK ASDSADSA";
        assertEquals(output, expected);
    }

    @Test
    void testSize() {
        taskList.add(task1);
        assertEquals(1, taskList.size());
        taskList.add(task2);
        assertEquals(2, taskList.size());
    }
}
