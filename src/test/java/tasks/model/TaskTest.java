package tasks.model;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.jupiter.api.Test;

// Test using JUnit 5
public class TaskTest {
    @Test
    void testEquals() {
        Task task1 = new Task("title", new Date(0));
        Task task2 = new Task("title", new Date(0));
        assertEquals(task1, task2);
    }

    @Test
    void testGetDateFormat() {
        SimpleDateFormat sdf = Task.getDateFormat();
        assertEquals("yyyy-MM-dd HH:mm", sdf.toPattern());
    }

    @Test
    void testGetEndTime() {
        Task task = new Task("title", new Date(0));
        assertEquals(new Date(0), task.getEndTime());
    }

    @Test
    void testGetRepeatInterval() {
        Task task = new Task("title", new Date(0), new Date(0), 1);
        assertEquals(1, task.getRepeatInterval());
    }

    @Test
    void testGetStartTime() {
        Task task = new Task("title", new Date(0));
        assertEquals(new Date(0), task.getStartTime());
    }

    @Test
    void testGetTime() {
        Task task = new Task("title", new Date(0));
        assertEquals(new Date(0), task.getTime());
    }

    @Test
    void testGetTitle() {
        Task task = new Task("title", new Date(0));
        assertEquals("title", task.getTitle());
    }

    @Test
    void testHashCode() {
        Task task1 = new Task("title", new Date(0));
        Task task2 = new Task("title", new Date(0));
        assertEquals(task1.hashCode(), task2.hashCode());
    }

    @Test
    void testIsActive() {
        Task task = new Task("title", new Date(0));
        task.setActive(true);
        assertEquals(true, task.isActive());
    }

    @Test
    void testIsRepeated() {
        Task task = new Task("title", new Date(0), new Date(0), 1);
        assertEquals(true, task.isRepeated());
    }

    @Test
    void testSetActive() {
        Task task = new Task("title", new Date(0));
        task.setActive(true);
        assertEquals(true, task.isActive());
        task.setActive(false);
        assertEquals(false, task.isActive());
    }

    @Test
    void testSetTime() {
        Task task = new Task("title", new Date(0));
        task.setTime(new Date(0), new Date(0), 1);
        assertEquals(new Date(0), task.getTime());
    }

    @Test
    void testSetTime2() {
        Task task = new Task("title", new Date(0));
        task.setTime(new Date(0));
        assertEquals(new Date(0), task.getTime());
    }

    @Test
    void testSetTitle() {
        Task task = new Task("title", new Date(0));
        task.setTitle("new title");
        assertEquals("new title", task.getTitle());
    }
}
