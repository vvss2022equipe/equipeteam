package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
//import tasks.model.TasksOperations;

import java.util.*;

public class TasksService {

    private ArrayTaskList tasks;
    public static final Logger logger = LogManager.getLogger(TasksService.class);

    public TasksService(ArrayTaskList tasks){
        this.tasks = tasks;
    }


    public ObservableList<Task> getObservableList(){
        return FXCollections.observableArrayList(tasks.getAll());
    }

    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }

    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public Iterable<Task> incomingTasks(Date start, Date end){

        logger.info(start);
        logger.info(end);
        ArrayList<Task> incomingTasks = new ArrayList<>();
        int poz=0;
        if (start.before(end)) {
            while(poz<tasks.size()) {
                Task t = tasks.getTask(poz);
                poz++;
                if (t !=null) {
                    Date nextTime = t.nextTimeAfter(start);
                    if (nextTime != null) {
                        if (nextTime.before(end) || nextTime.equals(end)) {
                            incomingTasks.add(t);
                            logger.info(t.getTitle());
                        }
                    }
                }
            }
        }
        return incomingTasks;
    }
    public SortedMap<Date, Set<Task>> calendar(Date start, Date end){
        Iterable<Task> incomingTasks = incomingTasks(start, end);
        TreeMap<Date, Set<Task>> calendar = new TreeMap<>();

        for (Task t : incomingTasks){
            Date nextTimeAfter = t.nextTimeAfter(start);
            while (nextTimeAfter!= null && (nextTimeAfter.before(end) || nextTimeAfter.equals(end))){
                if (calendar.containsKey(nextTimeAfter)){
                    calendar.get(nextTimeAfter).add(t);
                }
                else {
                    HashSet<Task> oneDateTasks = new HashSet<>();
                    oneDateTasks.add(t);
                    calendar.put(nextTimeAfter,oneDateTasks);
                }
                nextTimeAfter = t.nextTimeAfter(nextTimeAfter);
            }
        }
        return calendar;
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        int result = (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
        return result;
    }

    public Iterable<Task> filterTasks(Date start, Date end){
        //TasksOperations tasksOps = new TasksOperations(getObservableList());
        Iterable<Task> filtered = incomingTasks(start,end);
        //Iterable<Task> filtered = tasks.incoming(start, end);

        return filtered;
    }
}
