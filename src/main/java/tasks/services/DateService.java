package tasks.services;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateService {
    public static final int SECONDS_IN_MINUTE = 60;
    public static final int MINUTES_IN_HOUR = 60;
    public static final int HOURS_IN_A_DAY = 24;

    private TasksService service;

    public DateService(TasksService service) {
        this.service = service;
    }

    public static LocalDate getLocalDateValueFromDate(Date date) {//for setting to DatePicker - requires LocalDate
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

    }

    public Date getDateValueFromLocalDate(LocalDate localDate) {//for getting from DatePicker
        try{
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            return Date.from(instant);
        }
        catch (Exception error){
            throw new IllegalArgumentException("invalid date format");
        }
    }

    public Date getDateMergedWithTime(String time, Date noTimeDate) {//to retrieve Date object from both DatePicker and time field
        Pattern pattern = Pattern.compile("^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(time);
        boolean matchFound = matcher.find();
        if (!matchFound) {
            throw new IllegalArgumentException("invalid time format");
        }
        String[] units = time.split(":");
        int hour = Integer.parseInt(units[0]);
        int minute = Integer.parseInt(units[1]);
        if (hour > HOURS_IN_A_DAY || minute > MINUTES_IN_HOUR)
            throw new IllegalArgumentException("time unit exceeds bounds");
        Calendar calendar = GregorianCalendar.getInstance();
        try {

            calendar.setTime(noTimeDate);
        } catch (Exception error) {
            throw new IllegalArgumentException("invalid date format");
        }
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        return calendar.getTime();
    }

    public String getTimeOfTheDayFromDate(Date date) {//to set in detached time field
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);

        return service.formTimeUnit(hours) + ":" + service.formTimeUnit(minutes);
    }


}
