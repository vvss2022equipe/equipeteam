package tasks.model;

import java.util.Iterator;
import java.util.NoSuchElementException;
import org.apache.log4j.Logger;

abstract class TaskIterator implements Iterator<Task> {
    private int cursor;
    protected int lastCalled = -1;

    abstract Task getTaskFromList(int cursor);
    abstract void removeTaskFromList(Task task);
    abstract int getNumberOfTasks ();
    abstract Logger getLog();

    @Override
    public boolean hasNext() {
        return cursor < getNumberOfTasks();
    }

    @Override
    public Task next() {
        if (!hasNext()){
            getLog().error("next iterator element doesn't exist");
            throw new NoSuchElementException("No next element");
        }
        lastCalled = cursor;
        return getTaskFromList(cursor++);
    }

    @Override
    public void remove() {
        if (lastCalled == -1){
            throw new IllegalStateException();
        }
        removeTaskFromList(getTaskFromList(lastCalled));
        cursor = lastCalled;
        lastCalled = -1;
    }
}