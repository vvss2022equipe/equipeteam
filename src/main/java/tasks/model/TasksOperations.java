//package tasks.model;
//
//import javafx.collections.ObservableList;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//
//import java.util.*;
//
//public class TasksOperations {
//
//    public static final Logger logger = LogManager.getLogger(TasksOperations.class);
//    private List<Task> tasks;
//
//    public TasksOperations(ObservableList<Task> tasksList){
//        tasks=new ArrayList<>();
//        tasks.addAll(tasksList);
//    }
//
//    public Iterable<Task> incomingTasks(Date start, Date end){
//
//        logger.info(start);
//        logger.info(end);
//        ArrayList<Task> incomingTasks = new ArrayList<>();
//        for (Task t : tasks) {
//            Date nextTime = t.nextTimeAfter(start);
//            if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
//                incomingTasks.add(t);
//                logger.info(t.getTitle());
//            }
//        }
//        return incomingTasks;
//    }
//    public SortedMap<Date, Set<Task>> calendar( Date start, Date end){
//        Iterable<Task> incomingTasks = incomingTasks(start, end);
//        TreeMap<Date, Set<Task>> calendar = new TreeMap<>();
//
//        for (Task t : incomingTasks){
//            Date nextTimeAfter = t.nextTimeAfter(start);
//            while (nextTimeAfter!= null && (nextTimeAfter.before(end) || nextTimeAfter.equals(end))){
//                if (calendar.containsKey(nextTimeAfter)){
//                    calendar.get(nextTimeAfter).add(t);
//                }
//                else {
//                    HashSet<Task> oneDateTasks = new HashSet<>();
//                    oneDateTasks.add(t);
//                    calendar.put(nextTimeAfter,oneDateTasks);
//                }
//                nextTimeAfter = t.nextTimeAfter(nextTimeAfter);
//            }
//        }
//        return calendar;
//    }
//}
//
